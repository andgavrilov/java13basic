package week9.oop3.icecreamfactory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA
}
