package professional.TU.week4.streams;

import java.util.List;
import java.util.function.Consumer;

public class StreamsExample {
    public static void main(String[] args) {
        List<String> myPlaces = List.of("Nepal, Kathmandu", "Nepal, Pokhara", "India, Delhi", "USA, New York", "Africa, Nigeria");
        
        myPlaces
              .stream()
              .filter((p) -> p.startsWith("Nepal"))
              //.map((p) -> p.toUpperCase())
              .map(String::toUpperCase)
              //выражение :: является ссылкой на метод, который эквивалентен лямбда-выражению (p) -> p.toUpperCase()
              .sorted()
              .forEach(System.out::println);
              //.forEach((p) -> System.out.println(p));
        
    }
}
