package professional.TU.week4.functional.task5;

/*
Создать параметризованный функциональный интерфейс (Generics)
На выходе получить:
1) Строку наоборот
2) Факториал числа
 */
public class Main {
    public static void main(String[] args) {
        //строку наоборот
        MyGenericInterface<String> reverseString = str -> new StringBuilder(str).reverse().toString();
        
        //Факториал числа
        MyGenericInterface<Integer> fact = (n) -> {
            int result = 1;
            for (int i = 1; i <= n; i++) {
                result *= i;
            }
            return result;
        };
        
        System.out.println("Строка наоборот: " + reverseString.func("Lambda"));
        System.out.println("факториал: " + fact.func(5));
    }
}
