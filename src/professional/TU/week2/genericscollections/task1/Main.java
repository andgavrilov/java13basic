package professional.TU.week2.genericscollections.task1;

/*
Создать класс Pair, который умеет хранить два значения:
1) Любого типа: Pair<T, U>
2) Одинакового: типа Pair<T>
3) Первый тип - только строка, второй - только число.
 */
public class Main {
    public static void main(String[] args) {
        //Пример на реализацию любого типа данных в классе Pair
//        Pair<String, Double> pair = new Pair<>();
//        pair.first = "Test String";
//        pair.second = 1.2d;
//        pair.print();
//
//        Pair<String, String> pair1 = new Pair<>();
//        pair1.first = "Test1";
//        pair1.second = "Test2";
//        pair1.print();
        //Пример на реализацию одинакового типа данных в классе Pair
//        Pair<String> pair = new Pair<>();
//        pair.first = "abc";
//        pair.second = "def";
//        pair.print();
        //Пример на реализацию Первый тип - только строка, второй - только число в классе Pair
        Pair<String, Double> pair = new Pair<>();
        pair.first = "abc";
        pair.second = 123d;
        pair.print();
    }
}
