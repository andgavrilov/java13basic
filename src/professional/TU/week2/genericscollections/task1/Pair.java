package professional.TU.week2.genericscollections.task1;

public class Pair<T extends String, U extends Number> {
    public T first;
    public U second;
    
    public void print() {
        System.out.println("First: " + first + ", Second: " + second);
    }
}
