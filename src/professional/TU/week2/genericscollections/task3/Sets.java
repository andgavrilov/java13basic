package professional.TU.week2.genericscollections.task3;

import java.util.HashSet;
import java.util.Set;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        System.out.println(set1);
        
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        //полезный метод для пересечения
        set1.retainAll(set2);
        
        //foreach
        for (Integer elem : set1) {
            System.out.println(elem);
        }
        //пример на лямбды
        //set1.forEach(System.out::println);
        //метод true/false есть ли хоть один элемент нашей текущей коллекции есть в другой коллекции
        //Collections.disjoint();
    }
}
