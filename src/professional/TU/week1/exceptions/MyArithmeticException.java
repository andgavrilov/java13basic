package professional.TU.week1.exceptions;

public class MyArithmeticException
      extends Exception {
    // extends ArithmeticException {
    public MyArithmeticException() {
        super("произошло деление на 0");
    }
    
    public MyArithmeticException(String message) {
        super(message);
    }
}
