package professional.TU.week3.annotations;

public interface Summable {
    int sum(int a, int b);
}
