package professional.TU.week3.reflection;

/*
Рефлексия (англ. reflection) – знание кода о самом себе
К рефлексии можно отнести возможность проитерироваться по всем полям класса
или найти и создать объект класса, по имени, заданному через текстовую строку

Class - основной класс для рефлексии с Java.
 */
public class Reflection {
    /*
    получение объектов типа Class
     */
    public static void main(String[] args) {
        //1 способ
        //поле .class - псевдополе! ( не настоящее поле )
        Class<String> c1 = String.class;
        
        //2 способ
        //метод getClass() класса Object
        //Этот метод учитывает полиморфизм и возвращает реальный класс, которым является объект
        CharSequence seq = "My String";
        Class<? extends CharSequence> c2 = seq.getClass(); //вернется string class
        System.out.println(c2);
        
        //3 способ
        //Найти этот класс по строчному имени с помощью статического метода Class::forName
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
            System.out.println(integerClass);
        }
        catch (ClassNotFoundException e) {
            System.out.println("Класс не может быть найден!");
        }
    }
}
