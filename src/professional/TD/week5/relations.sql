--Связи с таблицами
-- 1 - 1
-- 1 - M (M - 1)
-- M - M

-- книги          авторы                  авторы_книг
-- id              id                  id    id_книги id_автора
-- 1               3                   1      1        3
-- 2               4                   2      2        3
-- 3               6                   3      1        6

--Создадим таблицу Reviews.
--У одной книги может быть несколько оценок.

CREATE TABLE IF NOT EXISTS reviews
(
    id       bigserial primary key,
    book_id  integer REFERENCES books (id),
    reviewer varchar(100) NOT NULL,
    rating   integer      NOT NULL,
    comment  text         NULL
);

--Попробуем добавить отзыв к книге, айдишника которого не существует (foreign key не даст)
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (777, 'Петя', 9, 'отличная книга');

INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (1, 'Петя', 10, 'отличная книга');
INSERT INTO reviews(book_id, reviewer, rating)
VALUES (1, 'Кирилл', 9);
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (3, 'Петя', 7, 'ок');
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (4, 'Иннокентий', 2, 'не понравилась');

select *
from reviews;


--Достать только те записи reviews, у которых comment != null
select *
from reviews
where comment is not null;

--Посчитать сколько всего записей в reviews. Назвать столбец Количество отзывов
select count(*) as "Количество отзывов"
from reviews;

--Узнать количество уникальных id книг, по которым были оставлены отзывы:
select count(distinct book_id)
from reviews;

--Вывести сколько review по каждой id книги
select book_id, count(*)
from reviews
group by book_id;

--Вывести все значения по books и по reviews (объединение столбцов результатов)
select *
from reviews r,
     books b
where r.book_id = b.id;
--join
select *
from reviews r join books b on b.id = r.book_id;

--А через left (right) join добъемся вывода книги, для которой нет отзывов
select *
from reviews r right join books b on b.id = r.book_id;

--Вычислить среднюю оценку по каждой книге. Вывести столбцы Оценка, Название книги.
select avg(r.rating) "Оценка", b.title as "Название книги"
from books b join reviews r on b.id = r.book_id
group by b.title;