--Обновление данных
--Обновить время добавления для книги с id = 2

update books
set date_added = '2020-02-02'
where id = 2;

rollback;
commit;

select *
from books;