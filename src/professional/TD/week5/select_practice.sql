drop table books;
create table books
(
    id         serial primary key,
    title      varchar(100) NOT NULL,
    author     varchar(100) NOT NULL,
    date_added timestamp    NOT NULL
);
INSERT INTO books(title, author, date_added)
VALUES ('Недоросль', 'Д. И. Фонвизин', now());
INSERT INTO books(title, author, date_added)
VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
INSERT INTO books(title, author, date_added)
VALUES ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
INSERT INTO books(title, author, date_added)
VALUES ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());

--1. Достать запись под id = 2
select *
from books
where id = 2;

--ограничение на выходе по количеству
select *
from books
order by date_added
limit 2;

--2. Найти автора книги по названию ‘Недоросль’
select *
from books
where title = 'Недоросль';

--3. Найти все книги Пастернака
select *
from books
where lower(trim(author)) like lower('%Пастернак');

--4. Вывести максимальный id в таблице
select max(id)
from books;
select count(*)
from books;
--avg, min, max, sum, mod, power....

--5. Найти все книги Радищева или Пастернака отсортированные по дате в обратном порядке
select *
from books
where lower(trim(author)) like lower('%Пастернак') or upper(trim(author)) like upper('%Радищев')
/*union all
select *
from books
where upper(trim(author)) like upper('%Радищев')
 */
order by date_added desc;
--union, union all,

--6. Найти все книги Пастернака добавленные вчера
select *
from books
where lower(trim(author)) like lower('%Пастернак') and date_added <= now() - interval '24h';