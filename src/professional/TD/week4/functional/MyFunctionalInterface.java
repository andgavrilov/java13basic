package professional.TD.week4.functional;

@FunctionalInterface
public interface MyFunctionalInterface {
    //один метод
    double getValue();
    //Single Abstract Methods (SAM)
}
