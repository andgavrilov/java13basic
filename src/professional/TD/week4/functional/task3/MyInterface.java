package professional.TD.week4.functional.task3;

@FunctionalInterface
public interface MyInterface {
    
    double getPiValue();
}
