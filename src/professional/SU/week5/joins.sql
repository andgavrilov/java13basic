--Создадим таблицу Reviews.
--У одной книги может быть несколько оценок.

-- 1 - 1
-- 1 - M (M - 1)
-- M - M
--M:M реализация:
-- книги           авторы                      авторы_книг
-- id              id                  id    id_книги id_автора
-- 1               3                   1      1        3
-- 2               4                   2      2        3
-- 3               6                   3      1        6

CREATE TABLE IF NOT EXISTS reviews
(
    id       bigserial primary key,
    book_id  integer REFERENCES books (id),
    reviewer varchar(100) NOT NULL,
    rating   integer      NOT NULL,
    comment  text         NULL
);
--Попробуем добавить отзыв к книге, айдишника которого не существует (foreign key не даст)
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (777, 'Петя', 9, 'отличная книга');

INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (1, 'Петя', 10, 'отличная книга');

INSERT INTO reviews(book_id, reviewer, rating)
VALUES (1, 'Кирилл', 9);
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (3, 'Петя', 7, 'ок');
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (4, 'Иннокентий', 2, 'не понравилась');

select *
from reviews;

--Достать только те записи reviews, у которых comment != null
select *
from reviews
where comment is not null;

--Посчитать сколько всего записей в reviews. Назвать столбец Количество отзывов
select count(*) as "Количество отзывов"
from reviews;

--Узнать количество уникальных id книг, по которым были оставлены отзывы:
select count(distinct book_id)
from reviews;

--Вывести сколько review по каждой id книги
select book_id, count(*)
from reviews
group by book_id
order by 1;

--Вывести все значения по books и по reviews (объединение столбцов результатов)
select *
from books b,
     reviews as r
where b.id = r.book_id;

--joins
select *
from books b join reviews r on b.id = r.book_id;

--left/right join
select *
from books b left join reviews r on b.id = r.book_id;

--Вычислить среднюю оценку по каждой книге. Вывести столбцы Оценка, Название книги.
select b.title as "Название книги", avg(r.rating) as Оценка
from books b join reviews r on b.id = r.book_id
group by b.title