-- delete
-- truncate

-- drop

--Удалить элемент с id = 1
delete from books;
where id = 1;
select *
from books;

truncate table books;

drop table books;


create table books (id serial primary key,  title       varchar(100) NOT NULL,  author     varchar(100) NOT NULL, date_added timestamp   NOT NULL);
INSERT INTO books(title, author, date_added) VALUES ('Недоросль', 'Д. И. Фонвизин', now());
INSERT INTO books(title, author, date_added) VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
INSERT INTO books(title, author, date_added) VALUES ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
INSERT INTO books(title, author, date_added) VALUES ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());