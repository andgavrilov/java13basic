package professional.SU.week4.functional;


public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        //анонимный класс
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("я только что создал свой функциональный интерфейс");
            }
        }).start();
    
        new Thread(() -> System.out.println("я только что создал свой функциональный интерфейс")).start();
    }
}
