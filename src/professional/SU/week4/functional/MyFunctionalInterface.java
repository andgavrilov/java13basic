package professional.SU.week4.functional;

@FunctionalInterface
public interface MyFunctionalInterface {
    double getValue();
    //Single Abstract Methods (SAM)
}
