package professional.consult.profdz2.task1;

import java.util.*;

/*
Реализовать метод, который на вход принимает ArrayList<T>,
а возвращает набор уникальных элементов этого массива. Решить используя коллекции.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(0, 0, 1, 1, 2, 3, 4, 5, 5);
        convert(list);
        
    }
    
    public static <T> HashSet<T> convert(List<T> from) {
//        HashSet<T> result = new HashSet<>();
//        result.addAll(from);
//        return result;
        return new HashSet<>(from);
    }
}
