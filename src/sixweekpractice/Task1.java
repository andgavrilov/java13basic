package sixweekpractice;

import java.util.Scanner;

/*
Найдем факториал числа n рекурсивно.
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        int res = 1;
//        for (int i = 1; i <= n; i++) {
//            res = res * i;
//        }
//        System.out.println("Factorial: " + res);

//        int res = factorial(n);
//        System.out.println("Factorial via recursion: " + res);
        int res = factorialTail(n, 1);
        System.out.println(res);
    }
    
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
    
    public static int factorialTail(int n, int result) {
        if (n <= 1) {
            return result;
        }
        else {
            return factorialTail(n - 1, n * result);
        }
    }
}
